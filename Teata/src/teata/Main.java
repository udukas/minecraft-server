package teata;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player saatja = (Player) sender;
		String message = argsToString(Arrays.copyOfRange(args, 0, args.length));
		if (cmd.getName().equalsIgnoreCase("teata")){
		if (args.length == 0){
			saatja.sendMessage("[" + ChatColor.DARK_RED + "Teata" + ChatColor.WHITE + "]" +
		ChatColor.RED+ " Kasuta formaati: /teata <probleem>");
			return true;
		} 
			for(Player player : Bukkit.getServer().getOnlinePlayers()){
				if(player.hasPermission("abistaja.teata")){
				player.sendMessage("[" + ChatColor.RED + "Teata" + ChatColor.WHITE + "] " +
				saatja.getName()+ ": " + message + " [" + ChatColor.RED + saatja.getWorld().getName() + ChatColor.WHITE + "]");
				player.playSound(player.getLocation(), Sound.LEVEL_UP, 10, 1);
				}
			}
			
			
		}
		return true;
	}
	
	  public String argsToString(String[] args)
	  {
		  String message = "";
		    for (String part : args) {
		        if (message != "") message += " ";
		        message += part;
		    }
			return message;
	  }
	  //1 meetod
	  /*
	  public String argsToString(String[] args)
	  {
	    StringBuilder sb = new StringBuilder();
	    for (String current : args) {
	      sb.append(current + " ");
	    }
	    return sb.toString();
	  }
	  */
	  
	  //2 meetod
	  /*
	  public String argsToString(String[] args){
		  String text = "";
		  for (int i = 0; i < argSs.length; i ++){
			  text+= args[i] + " ";
		  }
		return text;
	  }
	  */
	}

	
	
	
	
