
package olkzzzzz;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

//Kalmer on hale skrub
public class Main extends JavaPlugin implements Listener {

    int sBIndex = 0;
    int sLength = 1;
    int taskid = 0;
    int runCount = 0;
    int currentAbistajas = 0;
    private Scoreboard board;
    private Objective o;
    private Score leht, leht1, kell, kell1, raha, raha1, abistajad1, ping, ping1;
    private SimpleDateFormat sdf;
    private boolean isBroadcastRunning = false;
    private String broadcast, color;

    public void onEnable() {
        sdf = new SimpleDateFormat("HH:mm");
        board = Bukkit.getScoreboardManager().getNewScoreboard();


        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player current : Bukkit.getOnlinePlayers()) {
                    if (o != null) o.unregister();
                    scoreboardInit();
                    sdf.setTimeZone(TimeZone.getTimeZone("GMT+2"));
                    kell = o.getScore(sdf.format(new Date(System.currentTimeMillis())));
                    kell.setScore(3);
                    if (!isBroadcastRunning) {
                        raha1 = o.getScore("Raha:");
                    } else {
                        raha1 = o.getScore("Sõnum:");
                        raha1.setScore(6);
                        String m = broadcast.substring(sBIndex, Math.min(sBIndex + 32 - color.length(), broadcast.length() - color.length()));
                        m = color + m;
                        if (m.length() <= color.length()) {
                            if (runCount < 3) {
                                runCount++;
                            } else {
                                isBroadcastRunning = false;
                                broadcast = null;
                                color = null;
                            }
                        } else {
                            sBIndex += sLength;
                            raha = o.getScore(m);
                            raha.setScore(5);
                        }
                    }

                    currentAbistajas = 0;
                    for (Player current1 : Bukkit.getOnlinePlayers()) {
                        if (current1.hasPermission("olen.abistaja123")) currentAbistajas++;
                }
                abistajad1 = o.getScore("Abistajaid: " + Integer.toString(currentAbistajas));
                abistajad1.setScore(5);
/*
                    int p;
                    if((p = getPing(current)) > 300){
                        ping = o.getScore(ChatColor.RED + String.valueOf(p));
                    } else if(p > 150){
                        ping = o.getScore(ChatColor.YELLOW + String.valueOf(p));
                    } else {
                        ping = o.getScore(ChatColor.GREEN + String.valueOf(p));
                    }
                    ping.setScore(6);*/
                    current.setScoreboard(board);
                }
            }
        }.runTaskTimer(this, 0, 200);
    }

    public void scoreboardInit() {

        o = board.registerNewObjective("UDUGAAS", "dummy");

        o.setDisplayName(ChatColor.GREEN + "= Craftly =");
        o.setDisplaySlot(DisplaySlot.SIDEBAR);

        leht1 = o.getScore(ChatColor.AQUA + "Koduleht: ");
        leht1.setScore(2);

        leht = o.getScore(ChatColor.AQUA + "craftly.eu");
        leht.setScore(1);

        kell1 = o.getScore(ChatColor.AQUA + "Kell on:");
        kell1.setScore(4);

        /*ping1 = o.getScore(ChatColor.AQUA + "Ping:");
        ping1.setScore(7);*/

    }

    public void scoreboardSet(Player p) {
        p.setScoreboard(board);
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e) {
        scoreboardSet(e.getPlayer());
    }


}