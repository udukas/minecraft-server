package CreativeMaailm;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Raamat extends JavaPlugin implements Listener {
		public ItemStack book;
		public void raamat(){
			//Raamat 
			book = new ItemStack(Material.WRITTEN_BOOK, 1);
	        
			//Leht 1
	        BookMeta meta = (BookMeta) book.getItemMeta();
	        meta.setTitle(ChatColor.YELLOW + "Creative - �petus");
	        meta.setLore(Arrays.asList(ChatColor.AQUA + "P�hilised teadmised " + ChatColor.YELLOW +
	        		 "Creative kohta"));
	        
	        meta.addPage(
	        		ChatColor.RED + "Tere tulemast " + ChatColor.YELLOW + "Creative" + ChatColor.RED + " maailma! \n" +
	        		ChatColor.DARK_GREEN + "See on raamat, mis �petab sind " + ChatColor.YELLOW + "Creative" + ChatColor.DARK_GREEN + "maailmas toime tulema.\n" +
	        		" \n" +
	        		"Keera lehti, et saada infot."
	        				);
	        //Leht 2
	        meta.addPage(
	        		ChatColor.GOLD + "ABI" +
					ChatColor.DARK_GREEN + "/plotme auto - Valib automaatselt sulle platsi, kus saad hakata ehitama. \n" +
					"/plotme home - Viib sind sinu platsile.\n"+
					"/plotme comment - J�ta platsile kommentaar/abin�u.\n" +
					"/plotme biome <bioom> - Muudab platsi bioomi.\n" + 
					"/plotme comments - Loe kommentaare. \n" +
					"/plotme add <nimi> - Lisa m�ngija oma platsile. \n"+
					"/plotme remove <nimi> - Eemalda m�ngija oma platsilt. \n" +
					"/plotme deny <nimi> - Keela m�ngijal sinu platsile tulla. \n" +
					"/plotme deny <nimi> - Luba m�ngijal keda sa oled varem keelanud platsile tulla."
	        );
	        //Leht 3
	        meta.addPage(
	        		ChatColor.GOLD + "Kuidas kaitsta oma ehitist\n"+
					ChatColor.DARK_AQUA + " asd"
	        );       
	        book.setItemMeta(meta);
		}
		
		
	

}
