package CreativeMaailm;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	private final static String PLUGIN_NAME = "MagicHorses";
	FileConfiguration mConf = null;
	List<String> optedOut = null;
	Raamat mRaamat = null;
	public void onEnable()
	  {
		mConf = this.getConfig();
	    Bukkit.getServer().getPluginManager().registerEvents(this, this);
	    Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + PLUGIN_NAME + ChatColor.WHITE + "]" + 
	      ChatColor.RED + " working");
	    mRaamat = new Raamat();
	    mRaamat.raamat();
	   new Holograms().MasterHolograms();
	   optedOut = mConf.getStringList("opt-out");
	  }
	public void onPlayerTeleport(PlayerTeleportEvent e) {
		Player p = e.getPlayer();
		teleOrJoin(p);
	}
	public void onPlayerLogin(PlayerLoginEvent e){
		teleOrJoin(e.getPlayer());
	}
	
	private void teleOrJoin(Player p){
		World playeriMaailm = p.getWorld();
		if(playeriMaailm == Bukkit.getServer().getWorld("creative")){
			if(!optedOut.contains(p.getName())){
				p.getInventory().setItem(1, mRaamat.book);
				optedOut.add(p.getName());
				mConf.set("opt-out", optedOut);
			}
		}
	}
}
