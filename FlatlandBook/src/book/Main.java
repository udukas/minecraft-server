package book;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	ItemStack book;
	public void onEnable(){
		Bukkit.getServer().getPluginManager().registerEvents(this, this);
		Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Flatland book" + ChatColor.WHITE + "]" + 
		ChatColor.RED + " WORKING!");
		
		//Raamat 
		book = new ItemStack(Material.WRITTEN_BOOK, 1);
        
		//Leht 1
        BookMeta meta = (BookMeta) book.getItemMeta();
        meta.setTitle(ChatColor.GREEN + "Flatland - �petus");
        meta.setLore(Arrays.asList(ChatColor.AQUA + "P�hilised teadmised "
        		+ "FlatLand survival kohta"));
        meta.addPage(

        				);
        //Leht 2
        meta.addPage(
        		ChatColor.GOLD + "P�him�te\n"+
				ChatColor.DARK_AQUA + " asd"
        );
        //Leht 3
        meta.addPage(
        		ChatColor.GOLD + "Kuidas kaitsta oma ehitist\n"+
				ChatColor.DARK_AQUA + " asd"
        );       
        book.setItemMeta(meta);
	}
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player saatja = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("flatland")){
			saatja.getInventory().addItem(book);
		}
		return true;
	}
	
}