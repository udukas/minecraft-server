package playtime;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {
	long ticks;
	  public void onEnable()
	  {
	    Bukkit.getServer().getPluginManager().registerEvents(this, this);
	   getLogger().info(ChatColor.WHITE + "[" + ChatColor.AQUA + "PlayTime" + ChatColor.WHITE + "]" + 
	      ChatColor.RED + " working");

	  }
	  

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    Player pl = (Player)sender;
	    if (cmd.getName().equalsIgnoreCase("manguaeg")) {
	      if (pl.hasPermission("manguaeg"))
	      {
	    	  if (args.length < 1)
	          {
	            pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "M�nguaeg" + ChatColor.WHITE + "]" + 
	            ChatColor.RED + " Kasuta formaati: /manguaeg <nimi>");
	            return true;
	          }
	    	  
	    	  Player p = Bukkit.getServer().getPlayer(args[0]);
	    	  OfflinePlayer offlinePlayer = Bukkit.getServer().getOfflinePlayer(args[0]);
	    	  if (p == null && offlinePlayer == null){
		            pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "M�nguaeg" + ChatColor.WHITE + "]" + 
		    	            ChatColor.RED + " Seda m�ngijat pole olemas.");
	    		  return true;
	    	  }
	    	  if(p.isOnline()){
	    		  ticks = p.getStatistic(Statistic.PLAY_ONE_TICK);
	    	  } else {
	    		  ticks = offlinePlayer.getPlayer().getStatistic(Statistic.PLAY_ONE_TICK);
	    	  }
	    	  
	    	  

	    	  double hour = (double) (ticks / 20 / 60 / 60);
	    	  pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "M�nguaeg" + ChatColor.WHITE + "] " + ChatColor.RESET + 
	    			  ChatColor.RED + "M�ngija " + ChatColor.RESET + args[0] + ChatColor.RED + " on siin serveris aega viitnud " + ChatColor.DARK_RED + hour + "H");
	      }
	    	}
		return true;
	    }

	}
