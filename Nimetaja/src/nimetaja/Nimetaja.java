package nimetaja;

import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class Nimetaja
  extends JavaPlugin
  implements Listener
{
	  public void onEnable()
	  {
	    Bukkit.getServer().getPluginManager().registerEvents(this, this);
	    Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "PlayTime" + ChatColor.WHITE + "]" + 
	      ChatColor.RED + " working");
	  }
  
  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
  {
    Player pl = (Player)sender;
    if (cmd.getName().equalsIgnoreCase("nimeta")) {
      if (pl.hasPermission("nimetaja"))
      {
        if (args.length == 0)
        {
          sender.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Nimetaja" + ChatColor.WHITE + "]" + 
            ChatColor.RED + " Kasuta formaati: /nimeta <nimi>");
          return true;
        }
        if (pl.getItemInHand().getType() == Material.AIR)
        {
          pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Nimetaja" + ChatColor.WHITE + "]" + 
            " Sul ei ole midagi k�es");
          return true;
        }
        ItemStack itemInHand = pl.getItemInHand();
        ItemMeta itemInHandMeta = itemInHand.getItemMeta();
        String nimi = argsToString((String[])Arrays.copyOfRange(args, 0, args.length));
        itemInHandMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&',ChatColor.RESET + nimi));
        itemInHand.setItemMeta(itemInHandMeta);
        pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Nimetaja" + ChatColor.WHITE + "]" + 
          " Su eseme nimi on n��d: " + ChatColor.translateAlternateColorCodes('&',ChatColor.RESET + nimi));
      }
      else
      {
        pl.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Nimetaja" + ChatColor.WHITE + "]" + 
          " Sul pole �iguseid selle k�skluse jaoks.");
      }
    }
    return false;
  }
  public String argsToString(String[] args)
  {
	  String message = "";
	    for (String part : args) {
	        if (message != "") message += " ";
	        message += part;
	    }
		return message;
  }
  //1 meetod
  /*
  public String argsToString(String[] args)
  {
    StringBuilder sb = new StringBuilder();
    for (String current : args) {
      sb.append(current + " ");
    }
    return sb.toString();
  }
  */
  
  //2 meetod
  /*
  public String argsToString(String[] args){
	  String text = "";
	  for (int i = 0; i < argSs.length; i ++){
		  text+= args[i] + " ";
	  }
	return text;
  }
  */
}
