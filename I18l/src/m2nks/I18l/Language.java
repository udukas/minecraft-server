package m2nks.I18l;

/**
 * Created by M2nks on 20.11.14.
 */
public enum Language {
    ET("et"), EN("en"), RU("ru");

    String code;
    private Language(String code){
        this.code = code;
    }

    public String getCode() {
        return code;
    }
    public static Language codeToLanguage(String code){
        if(code.equalsIgnoreCase("et")){
            return Language.ET;
        } else if(code.equalsIgnoreCase("en")){
            return Language.EN;
        } else if(code.equalsIgnoreCase("ru")){
            return Language.RU;
        }
        return null;
    }
}
