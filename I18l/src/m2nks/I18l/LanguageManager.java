package m2nks.I18l;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.logging.Level;

/**
 * Created by M2nks on 20.11.14.
 */
public class LanguageManager {

    private static Map<String, String> et = null;
    private static Map<String, String> en = null;
    private static Map<String, String> ru = null;

    private static Map<String, String> playerPrefs = null;

    public LanguageManager() {
        et = FileManager.scrapeLanguage(Language.ET);
        en = FileManager.scrapeLanguage(Language.EN);
        ru = FileManager.scrapeLanguage(Language.RU);
    }

    public String retrivePhrase(Language lang, String codename){
        switch(lang){
            case ET:
                if(!et.containsKey(codename)){
                    Main.getInstance().getLogger().log(Level.SEVERE, "MISSING PHRASE, CODENAMED:\""+codename+"\", LANGUAGE: \""+lang.getCode()+"\" ");
                    return ChatColor.RED + "INTERNAL ERROR!";
                }
                return et.get(codename);
            case EN:
                if(!en.containsKey(codename)){
                    Main.getInstance().getLogger().log(Level.SEVERE, "MISSING PHRASE, CODENAMED:\""+codename+"\", LANGUAGE: \""+lang.getCode()+"\" ");
                    return ChatColor.RED + "INTERNAL ERROR!";
                }
                return en.get(codename);
            case RU:
                if(!ru.containsKey(codename)){
                    Main.getInstance().getLogger().log(Level.SEVERE, "MISSING PHRASE, CODENAMED:\""+codename+"\", LANGUAGE: \""+lang.getCode()+"\" ");
                    return ChatColor.RED + "INTERNAL ERROR!";
                }
                return ru.get(codename);
        }
        return null;
    }
    public Language getPlayerLanguagePreference(Player p){
        if(playerPrefs == null) playerPrefs = FileManager.scrapePlayerPreferences();
        if(!playerPrefs.containsKey(p.getName())){
            return Language.ET;
        }
        return Language.codeToLanguage(playerPrefs.get(p.getName()));
    }
}
