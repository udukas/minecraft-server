package m2nks.I18l;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by M2nks on 21.11.14.
 */
public class I18l {
    private static LanguageManager lm = null;

    public I18l() {
        lm = new LanguageManager();
    }

    public static String tl(String codename, Language lang){
        if(lm == null) return null;
        return lm.retrivePhrase(lang, codename);
    }
    public static void sendMessage(Player pl, String codename){
        pl.sendMessage(tl(codename, lm.getPlayerLanguagePreference(pl)));
    }
    public static void broadcast(String codename){
        for(Player current: Bukkit.getOnlinePlayers()){
            sendMessage(current, codename);
        }
    }

}
