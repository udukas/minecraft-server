package m2nks.I18l;

import m2nks.I18l.commands.SetLanguage;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Main extends JavaPlugin {
    private static Plugin instance = null;
    FileConfiguration conf = null;
    File f;

    public static Plugin getInstance() {
        return instance;
    }

    public void onEnable() {
    	f = this.getDataFolder();
        getCommand("setlanguage").setExecutor(new SetLanguage());
        Map test = new HashMap<String, String>();
        test.put("iAmATestKey", "I am a translatable test value, you should be able to change in the config.");
        try {
            SLAPI.save(SLAPI.mapToList(test), "test.lang");
        } catch (Exception e) {
            e.printStackTrace();
        }
        test.clear();
        //TEST
        try {
			test = SLAPI.listToMap(SLAPI.load("test.lang"));
		} catch (Exception e) {
		}
        Bukkit.getServer().broadcastMessage((String) test.get("iAmATestKey"));
        if (test.get("iAmATestKey").equals("I am a translatable test value, you should be able to change in the config.")) Bukkit.getServer().broadcastMessage("SCRAPER WORKS");
        conf = getConfig();
        instance = this;
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                FileManager.savePlayerPrefs();
            }
        }, 1000L, 2000L);
    }

    public void onDisable() {
        FileManager.savePlayerPrefs();
        conf = null;
        instance = null;
    }

}
