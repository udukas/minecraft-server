package m2nks.I18l;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by M2nks on 20.11.14.
 * Manual rework on the Bukkit Demo.
 * Made to only return List.
 */
public class SLAPI {

    public static void save(List<String> list, String path) throws Exception
    {
        File file;
        if (!(file = new File(path)).exists()) {
            file.createNewFile();
        }
        FileWriter fw = new FileWriter(file);
        BufferedWriter bw = new BufferedWriter(fw);
        for (String str : list) {
            bw.write(str);
            bw.newLine();
        }
        bw.flush();
        bw.close();
    }

    public static List<String> load(String path) throws Exception
    {
        File file;
        if (!(file = new File(path)).exists()) {
            return null;
        }
        List<String> load = new ArrayList<String>();
        FileReader fr;

        fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        if (br == null) return null;

        while ((line = br.readLine()) != null) {
            load.add(line);
        }
        br.close();
        return load;
    }

    public static Map<String, String> listToMap(List<String> load) {
        Map<String, String> endResult = new HashMap<String, String>();

        for (String current : load) {
            String key = current.substring(1, current.indexOf("=") - 1);
            String value = current.substring(current.indexOf("=") + 2, current.length() - 1);
            endResult.put(key, value);
        }
        return endResult;
    }

    public static List<String> mapToList(Map<String, String> load) {
        List<String> delivery = new ArrayList<String>();
        for (int i = 0; i < load.size(); i++) {
            String endresult = "\"" + load.keySet().toArray()[i] + "\"=\"" + load.values().toArray()[i] + "\"";
            delivery.add(i, endresult);
        }
        return delivery;
    }
}

