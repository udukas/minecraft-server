package m2nks.I18l;

import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

/**
 * Created by M2nks on 20.11.14.
 */
public class FileManager {
	private static String DATA_FOLDER = Main.getInstance().getDataFolder().getAbsolutePath();
    private static String PLAYER_PREFS = DATA_FOLDER + "player.pref";
    private static String ET = DATA_FOLDER + "et.lang";
    private static String EN = DATA_FOLDER + "en.lang";
    private static String RU = DATA_FOLDER + "ru.lang";

    private static Map<String, String> et = null;
    private static Map<String, String> en = null;
    private static Map<String, String> ru = null;

    private static Map<String, String> playerPref = null;
    
    private static File et_file;
    private static File en_file;
    private static File ru_file;
    private static File player_pref_file;

    public FileManager(){
    	et_file = new File(ET);
    	en_file = new File(EN);
    	ru_file = new File(RU);
        player_pref_file = new File(PLAYER_PREFS);
        try{
    	if(!et_file.exists()) et_file.createNewFile();
    	if(!en_file.exists()) en_file.createNewFile();
            if (!ru_file.exists()) ru_file.createNewFile();
            if (!player_pref_file.exists()) player_pref_file.createNewFile();
        } catch(IOException e){}
    		
    	
    }
    public static Map<String/*key*/, String/*value*/> scrapeLanguage(Language lang){
        if(lang == Language.ET && et != null){
            return et;
        } else if(lang == Language.EN && en != null){
            return en;
        } else if(lang == Language.RU && ru != null){
            return ru;
        }
        /* REMOVED FOR TESTING PURPOSES, FUTURE SANITY CHECK
        if(!new File(ET).exists() || !new File(EN).exists() || !new File(RU).exists()){
            Main.getInstance().getLogger().log(Level.SEVERE, "LANGUAGES MISSING, CHECK FOR THE FILES!!!");
            Main.getInstance().getLogger().log(Level.SEVERE, "SHUTTING DOWN, TO PREVENT UNUSABLE SYSTEMS!!!");
            Main.getInstance().getServer().shutdown();
            return null;
        }*/
        Map<String, String> language = null;
        try {
            switch (lang) {
                case ET:
                    language = SLAPI.listToMap(SLAPI.load(ET));
                    et = language;
                case EN:
                    language = SLAPI.listToMap(SLAPI.load(EN));
                    en = language;
                case RU:
                    language = SLAPI.listToMap(SLAPI.load(RU));
                    ru = language;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return language;
    }

    public static Map<String, String> scrapePlayerPreferences() {
        if (playerPref != null) return playerPref;
        if (!new File(PLAYER_PREFS).exists()) {
            Main.getInstance().getLogger().log(Level.WARNING, "PLAYER PREFERENCES MISSING, NOT A HARD MISTAKE");
            Main.getInstance().getLogger().log(Level.WARNING, "CREATING NEW ONES NOW!");
            playerPref = new HashMap<String, String>();
            savePlayerPrefs();
        }
        try {
            playerPref = SLAPI.listToMap(SLAPI.load(PLAYER_PREFS));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return playerPref;
    }

    public static void changePlayerPrefs(Player p, Language lang){
        playerPref.put(p.getName(), lang.getCode());
    }
    public static void savePlayerPrefs(){
        try {
            SLAPI.save(SLAPI.mapToList(playerPref), PLAYER_PREFS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
