package m2nks.I18l.commands;

import m2nks.I18l.FileManager;
import m2nks.I18l.Language;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by M2nks on 21.11.14.
 */
public class SetLanguage implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage("Ingame ONLY!");
            return true;
        }
        Player p = (Player) commandSender;
        if(strings.length == 0 ||strings.length > 1) {
            commandSender.sendMessage("Usage: /setlanguage [et/en/ru]");
            return true;
        }
        if(strings[0].equalsIgnoreCase("et")){
            FileManager.changePlayerPrefs(p, Language.codeToLanguage("et"));
            commandSender.sendMessage("Eesti keel valitud!");
        }
        if (strings[0].equalsIgnoreCase("en")) {
        	FileManager.changePlayerPrefs(p, Language.codeToLanguage("en"));
        	commandSender.sendMessage("English Language chosen!");
        }
        if (strings[0].equalsIgnoreCase("ru")) {
        	FileManager.changePlayerPrefs(p, Language.codeToLanguage("ru"));
        	commandSender.sendMessage("Russki Jaz�k something");
        }

        return true;
    }
}
