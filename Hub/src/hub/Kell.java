package hub;

import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;

public class Kell implements Listener, CommandExecutor{ //
    public ItemStack creative, survival, pvpareen, flatland;
	public ItemStack kellItem;
	public ItemStack hub;
    public Inventory kell;
    private Plugin main;
    
    public Kell(Plugin instance){
    	main = instance;
    }
    
	public void onEnable(){
		Bukkit.getServer().getPluginManager().registerEvents(this, main);
		kell = Bukkit.getServer().createInventory(null, 9, ChatColor.RED + "Teleporteerumine");
		
		kellItem = new ItemStack(Material.WATCH);
		ItemMeta kellMeta = kellItem.getItemMeta();
		kellMeta.setDisplayName(ChatColor.AQUA + "Kell");
		kellMeta.setLore(Arrays.asList(ChatColor.GOLD + "Teleporteerumis vahend"));
		kellItem.setItemMeta(kellMeta);
		
		hub = new ItemStack(Material.WATER_BUCKET);
		ItemMeta hubMeta = hub.getItemMeta();
		hubMeta.setDisplayName(ChatColor.RED + "HUB");
		hubMeta.setLore(Arrays.asList(ChatColor.BLUE + "Teleporteeru HUB maailma."));
		hub.setItemMeta(hubMeta);
		
		creative = new ItemStack(Material.APPLE);
		ItemMeta creativeMeta = creative.getItemMeta();
		creativeMeta.setDisplayName(ChatColor.RED + "CREATIVE");
		creativeMeta.setLore(Arrays.asList(ChatColor.BLUE + "Teleporteeru CREATIVE maailma."));
		creative.setItemMeta(creativeMeta);
		
		
		survival = new ItemStack(Material.DIAMOND_PICKAXE);
		ItemMeta survivalMeta = survival.getItemMeta();
		survivalMeta.setDisplayName(ChatColor.RED + "SURVIVAL");
		survivalMeta.setLore(Arrays.asList(ChatColor.BLUE + "Teleporteeru SURVIVAL maailma."));
		survival.setItemMeta(survivalMeta);
		
		
		pvpareen = new ItemStack(Material.IRON_SWORD);
		ItemMeta pvpMeta = pvpareen.getItemMeta();
		pvpMeta.setDisplayName(ChatColor.RED + "PVP AREEN");
		pvpMeta.setLore(Arrays.asList(ChatColor.BLUE + "Teleporteeru PVP areenile"));
		pvpareen.setItemMeta(pvpMeta);
		
		flatland = new ItemStack(Material.GOLD_INGOT);
		ItemMeta flatlandMeta = flatland.getItemMeta();
		flatlandMeta.setDisplayName(ChatColor.RED + "FLATLAND SURVIVAL");
		flatlandMeta.setLore(Arrays.asList(ChatColor.BLUE + "Teleporteeru FlatLand survivalisse"));
		flatland.setItemMeta(flatlandMeta);
		
		kell.setItem(3, creative);
		kell.setItem(4, survival);
		kell.setItem(5, pvpareen);
		kell.setItem(6, flatland);
		kell.setItem(2, hub);
}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent x){
		Player pl = x.getPlayer();
			if(x.getAction() == Action.RIGHT_CLICK_AIR || x.getAction() == Action.RIGHT_CLICK_BLOCK || x.getAction() == Action.LEFT_CLICK_AIR || x.getAction() == Action.LEFT_CLICK_BLOCK){
				if(pl.getItemInHand().isSimilar(kellItem)){
					pl.openInventory(kell);
				}
			}
		}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player pl = (Player) sender;
		if (cmd.getName().equalsIgnoreCase("kell")){
			pl.getInventory().addItem(kellItem);
		}
		return true;
	}
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e){
		 if (!e.getInventory().getName().equalsIgnoreCase(kell.getName())) return;
		 e.setCancelled(true);
		 if (e.getCurrentItem().getItemMeta() == null) return;
         if (e.getCurrentItem().getItemMeta().getDisplayName().contains("CREATIVE")) {
                 World creativeM = Bukkit.getServer().getWorld("creative");
                 Location creative = new Location(creativeM, 0, 65, 0);
                 e.getWhoClicked().teleport(creative);
                 e.getWhoClicked().closeInventory();
         }
         if (e.getCurrentItem().getItemMeta().getDisplayName().contains(survival.getItemMeta().getDisplayName())) {
                 World survivalM = Bukkit.getServer().getWorld("world");
                 Location survival = new Location(survivalM, 0, 0, 0);
                 e.getWhoClicked().teleport(survival);
                 e.getWhoClicked().closeInventory();
         }
         if (e.getCurrentItem().getItemMeta().getDisplayName().contains(pvpareen.getItemMeta().getDisplayName())) {
                 //World survivalM = Bukkit.getServer().getWorld("world");
                 //Location PVPAreen = new Location(survivalM, 0, 0, 0); Pole veel valmis!
                 Player player = (Player) e.getWhoClicked();
                 player.sendMessage(ChatColor.WHITE + "[" + ChatColor.GREEN +"Kell" + ChatColor.WHITE+ "]"+                		 
                		 " PVP areen pole valmis");
             player.closeInventory();
         }
         if (e.getCurrentItem().getItemMeta().getDisplayName().contains(hub.getItemMeta().getDisplayName())) {
             World hub = Bukkit.getServer().getWorld("hub");
             Location hubAsukoht = new Location(hub, 217, 99, 348);
             e.getWhoClicked().teleport(hubAsukoht);
             e.getWhoClicked().closeInventory();
     }
         if (e.getCurrentItem().getItemMeta().getDisplayName().contains(flatland.getItemMeta().getDisplayName())) {
             World flatlandM = Bukkit.getServer().getWorld("flatland");
             Location flatland = new Location(flatlandM, 0, 0, 0);
             e.getWhoClicked().teleport(flatland);
             e.getWhoClicked().closeInventory();
     }
 }
	}

	
	