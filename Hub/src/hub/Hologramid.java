package hub;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.filoghost.holograms.api.HolographicDisplaysAPI;

public class Hologramid {
	
	Location flocation, slocation, tlocation, clocation;
	Plugin instance;
	
	public Hologramid(Plugin instance){
		 flocation = new Location(Bukkit.getServer().getWorld("hub"), 278.5, 72, 13.5); //Tulekul
		 slocation = new Location(Bukkit.getServer().getWorld("hub"), 264.5, 72, 27.5); //Survival
		 tlocation = new Location(Bukkit.getServer().getWorld("hub"), 250.5, 72, 13.5); //PlotMe/Creative
		 clocation = new Location(Bukkit.getServer().getWorld("hub"), 264.5, 72, -0.5); //FlatLand survival
		 this.instance = instance;
		 }
		 public void createHolos(){
		     HolographicDisplaysAPI.createHologram(
		              instance,
		              flocation,
		              ChatColor.GREEN + "═══════════════════",
		              ChatColor.RED + "TULEKUL!",
		              ChatColor.GREEN + "═══════════════════"
		              );
		     HolographicDisplaysAPI.createHologram(
		             instance,
		             slocation,
		             ChatColor.GREEN + "═══════════════════",
		             ChatColor.AQUA + "SURVIVAL",
		             ChatColor.GREEN + "═══════════════════"
		             );
		     HolographicDisplaysAPI.createHologram(
		             instance,
		             tlocation,
		             ChatColor.GREEN + "═══════════════════",
		             ChatColor.YELLOW + "CREATIVE",
		             ChatColor.GREEN + "═══════════════════"
		             );
		     HolographicDisplaysAPI.createHologram(
		             instance,
		             clocation,
		             ChatColor.GREEN + "═══════════════════",
		             ChatColor.GOLD + "FLATLAND SURVIVAL",
		             ChatColor.GREEN + "═══════════════════"
		             );
		 

}
}