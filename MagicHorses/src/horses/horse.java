package horses;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;

public class horse
  extends JavaPlugin
  implements Listener
{
	  private Player player;
	  private Inventory menu;
	  private ItemStack undeadH, skeleH;
	  
	  @SuppressWarnings("deprecation")
	public void onEnable()
	  {
	    Bukkit.getServer().getPluginManager().registerEvents(this, this);
	    Bukkit.getConsoleSender().sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "MagicHorses" + ChatColor.WHITE + "]" + 
	      ChatColor.RED + " working");
	    
	    undeadH = new ItemStack(397, 1, (short) 0);
        SkullMeta undeadHMETA = (SkullMeta) undeadH.getItemMeta();
        undeadHMETA.setDisplayName(ChatColor.GRAY + "Skeleton horse");
        undeadHMETA.setLore(Arrays.asList(ChatColor.DARK_GRAY + "Skeleton horse"));
        undeadH.setItemMeta(undeadHMETA);
        
	    skeleH = new ItemStack(397, 1, (short) 2);
        SkullMeta skeleHMETA = (SkullMeta) skeleH.getItemMeta();
        skeleHMETA.setDisplayName(ChatColor.GREEN + "Zombie horse");
        skeleHMETA.setLore(Arrays.asList(ChatColor.DARK_GREEN + "Zombie horse"));
        skeleH.setItemMeta(skeleHMETA);
	    
		menu = Bukkit.getServer().createInventory(null, 9, ChatColor.RED + "Hobused");
		menu.setItem(3, undeadH);
		menu.setItem(5, skeleH);
	  }
	  
		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
			Player pl = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("hobune")){
				if(pl.hasPermission("magichorses")){
					pl.openInventory(menu);
				}
				else{
					pl.sendMessage("Sul ei ole luba, seda kasutada! Osta VIP");
				}
			}
			return true;
		}
		@EventHandler
	    public void onInventoryClick(InventoryClickEvent e) {
				player = (Player) e.getWhoClicked();
	            if (!e.getInventory().getName().equalsIgnoreCase(menu.getName())) return;
	            if (e.getCurrentItem().getItemMeta() == null) return;

	            if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Skeleton horse")) {
	                    e.setCancelled(true);
	                    skeletonHorse();
	                    e.getWhoClicked().closeInventory();
	            }
	            if (e.getCurrentItem().getItemMeta().getDisplayName().contains("Zombie horse")) {
                    e.setCancelled(true);
                    zombieHorse();
                    e.getWhoClicked().closeInventory();
            }
	            
		}
		//commit
	  private void zombieHorse(){
		  Horse zhorse = (Horse) player.getWorld().spawnEntity(player.getLocation(), EntityType.HORSE);
		  zhorse.setVariant(Variant.UNDEAD_HORSE);
		  zhorse.setTamed(true);
		  zhorse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
		  zhorse.setHealth(20.0);
		  zhorse.setBreed(false);
		  zhorse.setJumpStrength(2);
		  zhorse.setPassenger(player);
		
	  }
	  private void skeletonHorse(){
		  Horse shorse = (Horse) player.getWorld().spawnEntity(player.getLocation(), EntityType.HORSE);
		  shorse.setVariant(Variant.SKELETON_HORSE);
		  shorse.setTamed(true);
		  shorse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
		  shorse.setHealth(20.0);
		  shorse.setJumpStrength(2);
		  shorse.setBreed(false);
		  shorse.setPassenger(player);
		  shorse.setAdult();
	  }
  
	  
}
